//
//  localDatabase.swift
//  Coqus
//
//  Created by Robin Sauma on 2018-11-28.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

import SQLite3
import Foundation


class LocalDB {
    static let db:LocalDB = LocalDB()
    
    private let db: OpaquePointer?
    
    
    //MARK: - init which opens db/create it if it already exists
    init(){
        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            .appendingPathComponent("coqus.sqlite")
        var db: OpaquePointer? = nil
        if sqlite3_open(fileURL.path,&db) == SQLITE_OK {
            print("succeded")
            self.db = db
        }else{
            print("db didn't open or wasn't created for some reason")
            self.db = nil
        }
        self.createTable()
    }
    
    //MARK: - create/drop table
    func createTable(){
        let createTableString = "CREATE TABLE IF NOT EXISTS Recipe(id TEXT,name TEXT,image TEXT,ingredients TEXT,instructions TEXT);"
        if sqlite3_exec(db,createTableString,nil,nil,nil) != SQLITE_OK{
            let err = String(cString: sqlite3_errmsg(self.db)!)
            print(err)
        }else{
            print("Recipe Table created")
        }
    }
    
    func dropTable(){
        let query = "DROP TABLE Recipe;"
        
        if sqlite3_exec(db,query,nil,nil,nil) != SQLITE_OK {
            print("couldn't dropt table")
        }else{
            print("dropped")
        }
        
    }
    
    //MARK: - insert to table
    func insert(_ id: String = "",_ name: String = "",_ imageObject: UIImage?,_ ingredients:String = "",_ instructions:String = ""){
        var stmt:OpaquePointer?
        
        var imageString:String = ""
        var image = imageObject
        if(image == nil){
            image = UIImage(named: "errorPIC")
        }
        
        let imageData = image!.pngData()
        imageString = imageData!.base64EncodedString()
        
        let SQLITE_TRANSIENT = unsafeBitCast(OpaquePointer(bitPattern: -1), to: sqlite3_destructor_type.self)
        
        let query = "INSERT INTO Recipe (id,name,image,ingredients,instructions) VALUES(?,?,?,?,?);"
        if sqlite3_prepare(db,query, -1,&stmt,nil) == SQLITE_OK {
            sqlite3_bind_text(stmt,1 , id, -1 ,SQLITE_TRANSIENT)
            sqlite3_bind_text(stmt, 2, name, -1, SQLITE_TRANSIENT)
            sqlite3_bind_text(stmt,3,imageString,-1,SQLITE_TRANSIENT)
            sqlite3_bind_text(stmt,4,ingredients,-1,SQLITE_TRANSIENT)
            sqlite3_bind_text(stmt,5,instructions,-1,SQLITE_TRANSIENT)
            if(sqlite3_step(stmt) == SQLITE_DONE){
                print("insert success")
            }
        }
        sqlite3_finalize(stmt)
    }
    
    //MARK: - read from table, all and one
    func readSpecific(_ id:String) -> [String]{
        let query = "SELECT * FROM recipe where id = '" + String(id) + "';"
        var stmt:OpaquePointer?
        
        var readArray:[String] = []
        
        if sqlite3_prepare(db,query,-1,&stmt,nil) != SQLITE_OK {
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error preparing readSpecific: \(errmsg)")
        }
        
        if(sqlite3_step(stmt) == SQLITE_ROW){
            let queryResultCol1 = sqlite3_column_text(stmt, 1)
            let name = String(cString: queryResultCol1!)
            
            let queryResultCol2 = sqlite3_column_text(stmt, 2)
            let image = String(cString: queryResultCol2!)
            
            let queryResultCol3 = sqlite3_column_text(stmt, 3)
            let ingredients = String(cString: queryResultCol3!)
            
            let queryResultCol4 = sqlite3_column_text(stmt, 4)
            let instructions = String(cString: queryResultCol4!)
            
            readArray.append(name)
            readArray.append(image)
            readArray.append(ingredients)
            readArray.append(instructions)
        }else{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error reading readSpecific: \(errmsg)")
        }
        
        sqlite3_finalize(stmt)
        return readArray
        
    }
 
    func readAll() -> [[String]] {
        let query = "SELECT * FROM Recipe;"
        
        var stmt:OpaquePointer?
        var ReadAllArray = [[String]]()
        
        if sqlite3_prepare(db,query,-1,&stmt,nil) == SQLITE_OK{
            while(sqlite3_step(stmt) == SQLITE_ROW){
                let id =  String(cString: sqlite3_column_text(stmt, 0))
                let name = String(cString: sqlite3_column_text(stmt,1))
                let image = String(cString: sqlite3_column_text(stmt,2))
                let ingredients = String(cString: sqlite3_column_text(stmt,3))
                let instructions = String(cString: sqlite3_column_text(stmt,4))
                
                ReadAllArray.append([id,name,image,ingredients,instructions])
            }
        }else{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error reding table: \(errmsg)")
        }
        sqlite3_finalize(stmt)
        return ReadAllArray
    }
    
    //MARK: - delete from table and check if it exists from id
    func deleteFromTable(id:String) {
        let query = "DELETE FROM Recipe where id = '" + String(id) + "';"
        
        var stmt:OpaquePointer?
        
        if sqlite3_prepare(db, query, -1, &stmt, nil) == SQLITE_OK{
            if(sqlite3_step(stmt) == SQLITE_DONE){
                print("deleted row with id" + String(id))
            }
        }
        sqlite3_finalize(stmt)
        
    }
    
    func checkIfExist(_ id:String) -> Bool {
        let query = "SELECT id FROM Recipe where id = '" + id + "';"
        
        var checkIfExistArr:[String] = []
        
        var stmt:OpaquePointer?
        
        if(sqlite3_prepare(db, query, -1, &stmt, nil)) == SQLITE_OK{
            while(sqlite3_step(stmt) == SQLITE_ROW){
                let id = String(sqlite3_column_int(stmt, 0))
                checkIfExistArr.append(id)
            }
        }else{
            let errmsg = String(cString: sqlite3_errmsg(db)!)
            print("error reding exitingItem: \(errmsg)")
        }
        sqlite3_finalize(stmt)
        if(checkIfExistArr.count > 0){
            return true
        }else{
            return false
        }
    }
    
}

