//
//  HomeViewController.swift
//  Coqus
//
//  Created by Milad Ziai on 2018-11-27.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var recipes: [DocumentSnapshot] = []
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        loadRecipes()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    //MARK: - table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell") as? recipeCellTableViewCell   {
            if let title = recipes[indexPath.row].data()!["title"] as? String    {
                if let imageName = recipes[indexPath.row].data()!["imageName"] as? String {
                    let currentCell = tableView.cellForRow(at: indexPath) as? recipeCellTableViewCell
                    if currentCell?.recipeTitle == nil {
                        database.downloadImage(imageName){ imageObject in
                            print("Downloading image")
                            cell.recipeImage.image = imageObject
                            cell.recipeTitle.text! = title
                        }
                    }
                }
            }
            return cell
        }else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Top Recipes"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)   {
        performSegue(withIdentifier: "detailViewControllerSegue", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? viewRecipeViewController  {
            if let indexPath = sender as? IndexPath  {
                destination.recipeInformation = recipes[indexPath.row]
                let selectedCell = tableView.cellForRow(at: indexPath) as! recipeCellTableViewCell
                destination.imageObject = selectedCell.recipeImage.image
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = recipes.count - 1
        if indexPath.row == lastItem    {
            print("Scrolling")
            loadRecipes()
        }
    }
    
    //MARK: Loads all recipes
    func loadRecipes()  {
        if recipes.count > 0    {
            database.getTopTenRecipes(lastDocument: (recipes[recipes.count - 1]))    { queryDocument in
                self.recipes.append(queryDocument)
                self.tableView.reloadData()
            }
        }   else    {
            database.getTopTenRecipes(lastDocument: (nil))    { queryDocument in
                self.recipes.append(queryDocument)
                self.tableView.reloadData()
            }
        }
    }
    //MARK: Add recipe action button
    @IBAction func recipeAction(_ sender: Any) {
        if database.getUserFromDatabase() != nil{
            self.performSegue(withIdentifier: "goToAddRec", sender: self)
        }else{
            tabBarController?.selectedIndex = 2
        }
    }
}
