//
//  previewViewController.swift
//  Coqus
//
//  Created by Victor Berggren on 2018-11-29.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class previewViewController: UIViewController {

    @IBOutlet weak var question: UIView!
    @IBOutlet weak var choosenPicture: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        question.layer.cornerRadius = 25
        
        
    }
    //MARK: - Buttons

    @IBAction func noButton(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func yesButton(_ sender: UIButton) {}
    
    
    //MARK - Unwind, send picture to addVC
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! AddViewController
        destinationVC.addPic = choosenPicture
    }
}
