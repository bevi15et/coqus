//
//  DatabaseClass.swift
//  Coqus
//
//  Created by Hassan Al-Baaj on 2018-11-26.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//
import Foundation
import Firebase
import FirebaseStorage
import FirebaseAuth


class databaseClass {
    var database: Firestore!
    var storage: Storage!
    var storageRef: StorageReference!

    init() {
        FirebaseApp.configure()
        database = Firestore.firestore()
        
        
        let settings = database.settings
        settings.areTimestampsInSnapshotsEnabled = true
        database.settings = settings
        
        storage = Storage.storage()
        storageRef = storage.reference()
    }
    
    //MARK: - push recipe to firebase
    func addNewRecipe(title: String, ingredients: [String], instructions: [String], completion: @escaping (_ documentName: String) -> Void )  {
        var ref: DocumentReference? = nil
        ref = database.collection("Recipes").addDocument(data: [:])
        database.collection("Recipes").document((ref?.documentID)!).setData([
            "imageName": ref?.documentID as Any,
            "title": title,
            "creatorID": (getUserFromDatabase()?.uid)!,
            "creatorName": (getUserFromDatabase()?.displayName)!,
            "ingredients": ingredients,
            "instructions": instructions,
            "favorites": 0 ])   { err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                    completion(ref!.documentID)
                }
        }
    }
    //MARK: - get top ten recipes from firebase
    func getTopTenRecipes(lastDocument: DocumentSnapshot?, completion: @escaping (_ querySnapShot: QueryDocumentSnapshot) -> Void)    {
        
        if lastDocument != nil  {
            database.collection("Recipes").order(by: "favorites", descending: true).limit(to: 10).start(afterDocument: lastDocument!).getDocuments()    { querySnapshot, error in
                if error != nil {
                    print("Error: \(String(describing: error))")
                }   else    {
                    for queryDocument in querySnapshot!.documents  {
                        print("Got Recipe!")
                        completion(queryDocument)
                    }
                }
            }
        }   else    {
            database.collection("Recipes").order(by: "favorites", descending: true).limit(to: 10).getDocuments()    { querySnapshot, error in
                if error != nil {
                    print("Error: \(String(describing: error))")
                }   else    {
                    for queryDocument in querySnapshot!.documents  {
                        print("Got Recipe!")
                        completion(queryDocument)
                    }
                }
            }
        }
    }
    func getRecipe(_ documentKey: String, completion: @escaping (_ document: DocumentSnapshot) -> Void) {
        database.collection("Recipes").document(documentKey).getDocument {(document, error) in
            if error != nil {
                print(error ?? "error")
            }   else    {
                completion(document!)
            }
        }
    }
    
    //MARK: - upload/download image to firebase storage
    func uploadImageFromImageObject(_ imageObject: UIImage, imageName: String, completion: @escaping (_ error: Error?) -> Void)    {
        let imageRef = storageRef.child("images/" + imageName)
        
        let compressedImageData = imageObject.jpegData(compressionQuality: 0.1)
        print("compressed image size = \(compressedImageData!)")
        _ = imageRef.putData(compressedImageData!, metadata: nil)    { metadata, error in
            guard metadata != nil else  {
                print("error \(String(describing: error))")
                return
            }
            completion(nil)
        }
    }

    func downloadImage(_ imageName: String, completion: @escaping (_ image: UIImage) -> Void)   {
        let imageRef = storageRef.child("images/" + imageName);
        imageRef.getData(maxSize: 200 * 1024 * 1024)  { data, error in
            if let error = error {
                print("error: \(error)")
            }   else    {
                let imageData = UIImage(data: data!)
                completion(imageData!)
            }
        }
    }
    
    //MARK: - Regristation and login
    func registerUser(userName: String, email: String, password: String, completion: @escaping (_ user: AuthDataResult?, _ error: Error?) ->Void) {
        Auth.auth().createUser(withEmail: email, password: password)    {   (user, error) in
            if error != nil {
                print("Error \(String(describing: error))")
                completion(user, error)
            }   else    {
                let changeRequest = Auth.auth().currentUser?.createProfileChangeRequest()
                changeRequest?.displayName = userName
                changeRequest?.commitChanges    { error in
                    if error != nil {
                        print("Error \(String(describing: error))")
                    }   else    {
                        completion(user, error)
                    }
                }
            }
        }
    }
    
    func loginUser(email: String, password: String, completion: @escaping (_ user: AuthDataResult?, _ error: Error?) -> Void)   {
        Auth.auth().signIn(withEmail: email, password: password)  { (user, error) in
            completion(user, error)
        }
    }
    
    func getUserFromDatabase() -> User? {
        return Auth.auth().currentUser
    }
    
    func logoutUser()   {
        do {
            try Auth.auth().signOut()
        }
        catch let signOutError as NSError{
            print ("Error signing out: %@", signOutError)
        }
    }
}
