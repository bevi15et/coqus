//
//  IngredientCell.swift
//  Coqus
//
//  Created by Milad Ziai on 2018-11-06.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class IngredientCell: UITableViewCell{
    
    @IBOutlet weak var ingredients: UILabel!
    
}
