//
//  recipeCellTableViewCell.swift
//  Coqus
//
//  Created by Hassan Al-Baaj on 2018-12-01.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class recipeCellTableViewCell: UITableViewCell {

    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    
}
