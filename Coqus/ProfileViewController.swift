//
//  ProfileViewController.swift
//  Coqus
//
//  Created by Milad Ziai on 2018-11-28.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    @IBOutlet weak var LoginLogoutbutton: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if database.getUserFromDatabase() != nil {
            signUpBtn.isHidden = true
            LoginLogoutbutton.setTitle("Logout", for: .normal)
        }   else    {
            signUpBtn.isHidden = false
            LoginLogoutbutton.setTitle("Login", for: .normal)
        }
    }
    
    //MARK: - Login and signup segues
    @IBAction func goToLoginButton(_ sender: Any) {
        if database.getUserFromDatabase() != nil {
            database.logoutUser()
            LoginLogoutbutton.setTitle("Login", for: .normal)
            signUpBtn.isHidden = false
        }   else    {
            self.performSegue(withIdentifier: "loginSegue", sender: self)
        }
    }

    @IBAction func goToSignUPButton(_ sender: Any) {
        self.performSegue(withIdentifier: "segToSignUP", sender: self)
    }
    
}
