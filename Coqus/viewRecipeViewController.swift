//
//  viewRecipeViewController.swift
//  Coqus
//
//  Created by Robin Sauma on 2018-11-28.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit
import Firebase

class viewRecipeViewController: UIViewController {

    var recipeInformation: DocumentSnapshot?
    
    @IBOutlet weak var creatorLabel: UILabel!
    @IBOutlet weak var recipeImageBG: UIImageView!
    @IBOutlet weak var recipeImageFront: UIImageView!
    @IBOutlet weak var ingredients: UILabel!
    @IBOutlet weak var instructions: UILabel!
    @IBOutlet weak var recipeTitle: UILabel!
    
    @IBOutlet weak var travelBox: UIView!
    @IBOutlet weak var saveButtonView: UIButton!
    @IBOutlet weak var recipeScrollView: UIScrollView!
    
    var imageObject: UIImage?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        var exists:Bool
        var strings:[String]
        var labels:[UILabel]
        
        //MARK: - retreive information from database through model
        (strings, labels , exists ) = viewcontrol.viewRecipe(recipeInformation!)
        
        recipeTitle.text = strings[0]
        creatorLabel.text = strings[1]
        ingredients.text = labels[0].text
        instructions.text = labels[1].text
        recipeImageFront.image = imageObject
        recipeImageBG.image = imageObject
        
        if(exists){
            saveButtonView.isHidden = true
        }
        
        //MARK: - BG-image blurred Front-image round
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = recipeImageBG.bounds
        recipeImageBG.addSubview(blurView)
        
        self.recipeImageFront.layer.cornerRadius = self.recipeImageFront.frame.size.width/2
        self.recipeImageFront.clipsToBounds = true

        travelBox.alpha = 0
        
        saveButtonView.layer.borderWidth = 5
        saveButtonView.layer.borderColor = UIColor.init(displayP3Red: 0.99, green: 0.62, blue: 0.05, alpha: 1.0).cgColor

        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
    }
    
    //MARK: - animation and insert for save button
    @IBAction func saveButtonPressed(_ sender: Any) {
        let IngredientsList = (recipeInformation!.data()?["ingredients"] as? [String])!.joined(separator: "€%,!=(")
        let InstructionList = (recipeInformation!.data()?["instructions"] as? [String])!.joined(separator: "€%,!=(")
        print(IngredientsList)
        print(InstructionList)
        localDB.insert((recipeInformation?.documentID)! ,recipeTitle.text! , imageObject, IngredientsList, InstructionList)
        
        
        UIView.animate(withDuration: 1, animations: {
            self.travelBox.alpha = 1.0
        })
        UIView.animate(withDuration: 1, delay: 0.7, options: [], animations: {
            self.saveButtonView.transform = CGAffineTransform(scaleX: 0.05, y: 0.5)
        }, completion: nil)
        UIView.animate(withDuration: 0.5, delay: 1.9, options: [], animations: {
            self.saveButtonView.frame.origin.y = self.travelBox.frame.origin.y
        }, completion: nil)
        UIView.animate(withDuration: 3, delay: 2.6, options: [], animations: {
            self.travelBox.frame.origin.y += self.recipeScrollView.frame.height
            self.saveButtonView.frame.origin.y += self.recipeScrollView.frame.height
        }, completion: nil)
        
        
    }
    
}
