//
//  viewLocalSavedRecipeViewController.swift
//  Coqus
//
//  Created by Robin Sauma on 2018-11-28.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class viewLocalSavedRecipeViewController: UIViewController {
    @IBOutlet weak var recipeImageBG: UIImageView!
    @IBOutlet weak var recipeImageFront: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var Ingredients: UILabel!
    @IBOutlet weak var Instructions: UILabel!
    
    var id:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.reloadInputViews()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        var images:UIImage
        var labels:[UILabel]
        Ingredients.numberOfLines = 0
        Instructions.numberOfLines = 0
        
        //MARK: - retreive information through controller
        (recipeTitle.text! , labels, images) = viewcontrol.viewRecipeLocal(id)
        Ingredients.text = labels[0].text
        Instructions.text = labels[1].text
        recipeImageBG.image = images
        recipeImageFront.image = images
        
        //MARK: - setting pictures to blurry and circle shaped
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = recipeImageBG.bounds
        recipeImageBG.addSubview(blurView)
        
        self.recipeImageFront.layer.cornerRadius = self.recipeImageFront.frame.size.width/2
        self.recipeImageFront.clipsToBounds = true
        
    }

}
