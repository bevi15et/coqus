//
//  SignUpViewController.swift
//  Coqus
//
//  Created by Milad Ziai on 2018-11-28.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var repeatPwTextField: UITextField!
    @IBOutlet weak var UsernameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
    }
    
    //MARK: - register and pop back if successfully
    @IBAction func signUpAction(_ sender: Any) {
        if passwordTextField.text != repeatPwTextField.text {
            
            let alertController = UIAlertController(title: "Error", message: "Passwords don't match!", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }else {
            database.registerUser(userName: UsernameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!)   { user, error in
                if error == nil {
                    self.popBack(2)
                }else{
                    let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        }
    }
    
    //after successfully you go back 
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: false)
                return
            }
        }
    }


}
