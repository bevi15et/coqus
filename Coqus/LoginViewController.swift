//
//  LoginViewController.swift
//  Coqus
//
//  Created by Milad Ziai on 2018-11-28.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround() 
    }
    //MARK: - try to login button tapped
    @IBAction func loginAction(_ sender: Any) {
        database.loginUser(email: emailTextfield.text!, password: passwordTextField.text!)  { user, error in
            if(database.getUserFromDatabase() != nil)    {
                self.popBack(2)
            }   else    {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    //after login successfully go back
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: false)
                return
            }
        }
    }
    
}

extension UIViewController {
    
    //MARK: - keyboard functionalities
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
