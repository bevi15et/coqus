//
//  viewRecipeViewControllerModel.swift
//  Coqus
//
//  Created by Robin Sauma on 2018-12-06.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import Foundation
import UIKit
import Firebase



class viewRecipeShowModel {
    
    //MARK: - getINFO for Global db
    func getInfo(_ recipeInformation: DocumentSnapshot) -> ([String],[UILabel],Bool){
        
        let IngredientsList = (recipeInformation.data()?["ingredients"] as? [String])!
        let InstructionList = (recipeInformation.data()?["instructions"] as? [String])!
        var ingLabel = UILabel()
        var insLabel = UILabel()
        
        ingLabel = getList(theLabel: ingLabel, list: IngredientsList)
        insLabel = getList(theLabel: insLabel, list: InstructionList)
       
        let recipeTitle = (recipeInformation.data()?["title"] as? String)!
        let creatorLabel = (recipeInformation.data()?["creatorName"] as? String)!
        
        
        return ([recipeTitle, creatorLabel],[ingLabel, insLabel], (localDB.checkIfExist(recipeInformation.documentID)))
        
    }
    
    //MARK: - getInfo from Local db
    func getInfoLocal(_ id:String) -> (String, [UILabel], UIImage){
        let theRecipe:[String] = localDB.readSpecific(id)
        
        let title = theRecipe[0]
        var localImage:UIImage
     
        let IngredientsList:[String] = theRecipe[2].components(separatedBy: "€%,!=(")
        let InstructionList:[String] = theRecipe[3].components(separatedBy: "€%,!=(")
        var ingLabel:UILabel = UILabel()
        var insLabel:UILabel = UILabel()
        
        //decoding image
        if let decodedData = Data(base64Encoded: theRecipe[1], options: NSData.Base64DecodingOptions.ignoreUnknownCharacters) {
            let decodedImage = UIImage(data: decodedData)
            localImage = decodedImage!
        }else{
            localImage = UIImage(named:"errorPIC")!
        }
        
        ingLabel = getList(theLabel: ingLabel, list: IngredientsList)
        insLabel = getList(theLabel: insLabel, list: InstructionList)
        
        return (title,[ingLabel,insLabel],localImage)
        
    }
    
    //MARK: - function for lists
    func getList(theLabel:UILabel, list:[String]) -> UILabel {
        theLabel.text = ""
        theLabel.numberOfLines = 0
        for x in list{
            theLabel.text?.append("\(x) \n\n")
        }
        return theLabel
    }
    
}
