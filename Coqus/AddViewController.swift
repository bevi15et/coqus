//
//  AddViewController.swift
//  Coqus
//
//  Created by Milad Ziai on 2018-11-25.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit
import Firebase

class AddViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource{

    
    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addIngredientTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var addInstructionTextField: UITextField!
    @IBOutlet weak var addPic: UIImageView!
    
    var Ingredients: [String] = []
    var Instructions: [String] = []
    var document: DocumentSnapshot?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        let tap = UITapGestureRecognizer(target: self, action: #selector(myViewTapped(_:)))
        addPic.addGestureRecognizer(tap)
        self.hideKeyboardWhenTappedAround() 
    }
    
    // MARK: - Alert message when error occur
    func alertMessage(_ message: String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    // MARK: -  add ingredients and instructions buttons tapped
    @IBAction func addIButtonTapped(_ sender: UIButton) {
        if addIngredientTextField.text == ""{
            alertMessage("Add an ingredient")
        }else {
            insertNewIngredient()
        }
    }
  
    @IBAction func addButton2Tapped(_ sender: UIButton) {
        if addInstructionTextField.text == ""{
            alertMessage("Add an Instruction")
        }else {
            insertNewInstruction()
        }
    }
    
    // MARK: - Insert text to table
    func insertNewIngredient()  {
        Ingredients.append(addIngredientTextField.text!)
        
        let indexPath = IndexPath(row: Ingredients.count - 1, section: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
        
        addIngredientTextField.text = ""
        view.endEditing(true)
    }
    
    func insertNewInstruction() {
        
        Instructions.append(addInstructionTextField.text!)
        
        let indexPath = IndexPath(row: Instructions.count - 1, section: 1)
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .fade)
        tableView.endUpdates()
        
        addInstructionTextField.text = ""
        view.endEditing(true)
    }
    
    // MARK: - saves recipe to the database
    @IBAction func saveToDB(_ sender: Any) {
        if database.getUserFromDatabase() != nil    {
            if titleTextField.text! == ""{
                alertMessage("Add a title, please!")
            }else if Ingredients.count < 1   {
                self.alertMessage("Add an ingredient, please!")
            }else if Instructions.count < 1 {
                self.alertMessage("Add an instruction, please!")
            }else {
                //Save recipe too database!
                database.addNewRecipe(title: self.titleTextField.text!, ingredients: self.Ingredients, instructions: self.Instructions) {documentName in
                    //Adds the image to Firebase Storage, localFilePath is the path to the image and imageName is the desired image name(ALWAYS use the documentName as imageName)
                    if let imageObject = self.addPic.image   {
                        database.uploadImageFromImageObject(imageObject, imageName: documentName)  { error in
                            print("Image uploaded")
                        }
                        database.getRecipe(documentName)    { document in
                            self.document = document
                            self.performSegue(withIdentifier: "addGoToDetailView", sender: self)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - ingredients and instructions Table view

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return Ingredients.count
        }else {
            return Instructions.count
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if section == 0{
            return "Ingredients"
        }else {
            return "Instructions"
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell") as? IngredientCell{
            
            if(indexPath.section == 0){
                let ingredients = Ingredients[indexPath.row]
                cell.ingredients.text = ingredients
            }else {
                let instructions = Instructions[indexPath.row]
                cell.ingredients.text = instructions
            }
            
        return cell
            
        }else {
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // MARK: - editing cells in sections
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete{
            if indexPath.section == 0{
                Ingredients.remove(at: indexPath.row)
            }else {
                Instructions.remove(at: indexPath.row)
            }
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
    //MARK: - Trigger get picture segue
    @objc func myViewTapped(_ sender: UITapGestureRecognizer){
        performSegue(withIdentifier: "pictureSegue", sender: self)
    }

    @IBAction func unwindToAdd(_ sender: UIStoryboardSegue) {}
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? viewRecipeViewController  {
            destination.recipeInformation = document
            destination.imageObject = self.addPic.image
        }
    }
}
