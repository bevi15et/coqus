import UIKit
import SafariServices
import UserNotifications
import Firebase
class ViewController{
    
    let model = viewRecipeShowModel()
    
    //MARK: func to viewRecipe model for online database
    func viewRecipe(_ recipeInformation:DocumentSnapshot) -> ([String] , [UILabel] , Bool ) {
        return model.getInfo(recipeInformation)
    }
    //MARK: func to viewRecipe model for local database
    func viewRecipeLocal(_ id:String) -> (String, [UILabel], UIImage){
        return model.getInfoLocal(id)
    }
    
}
