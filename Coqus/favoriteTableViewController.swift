//
//  favoriteTableViewController.swift
//  Coqus
//
//  Created by Robin Sauma on 2018-11-30.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class favoriteTableViewController: UITableViewController {
    
    var favoriteList:[[String]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favoriteList = localDB.readAll()
        tableView.reloadData()
    }
    
    //MARK: - table view functionality
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoriteList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "favoriteCell", for: indexPath) as? favoriteTableViewCell{
            let currentRecipe:[String] = favoriteList[indexPath.row]
            
            cell.id = currentRecipe[0]
            cell.favoriteLabelCell.text = currentRecipe[1]
            if let decodedData = Data(base64Encoded: currentRecipe[2], options: NSData.Base64DecodingOptions.ignoreUnknownCharacters) {
                let decodedImage = UIImage(data: decodedData)
                cell.favoriteImageCell.image = decodedImage
            }else{
                cell.favoriteImageCell.image = UIImage(named: "errorPIC")
            }
            
            return cell
        }else{return UITableViewCell()}
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Favorite"
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete{
            
            localDB.deleteFromTable(id: favoriteList[indexPath.row][0])
            favoriteList.remove(at: indexPath.row)

            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
    }
    
    //MARK: - Prepare and perform segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? viewLocalSavedRecipeViewController {
            if let indexPath = sender as? IndexPath {
                let recipeID:String = favoriteList[indexPath.row][0]
                destination.id = recipeID
            }
        }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "favoriteSegue", sender: indexPath)
    }
}
