//
//  recipeValues.swift
//  Coqus
//
//  Created by Robin Sauma on 2018-11-28.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import Foundation

struct RecipeInfo {
    var id:String
    var name:String
    var image:String
    var ingredientList:[String]
    var instructionList:[String]
}
