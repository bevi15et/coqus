//
//  PictureViewController.swift
//  Coqus
//
//  Created by Victor Berggren on 2018-11-23.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class PictureViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var chooseView: UIView!
    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var returnPic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chooseView.alpha = 0
        returnPic.alpha = 0
        
        buttonView.layer.cornerRadius = 25
        chooseView.layer.cornerRadius = 25
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(backToPrevious))
        
        transparentView.addGestureRecognizer(backTap)
        
    }
    //MARK: - Button action
    @IBAction func cameraButton(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imgPic = UIImagePickerController()
            imgPic.delegate = self
            imgPic.sourceType = .camera
            imgPic.allowsEditing = false
            self.present(imgPic, animated: true, completion: nil)
        }
    }
    
    @IBAction func galleryButton(_ sender: AnyObject) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imgPic = UIImagePickerController()
            imgPic.delegate = self
            imgPic.sourceType = .photoLibrary
            imgPic.allowsEditing = false
            self.present(imgPic, animated: true, completion: nil)
        }
    }
    
    @IBAction func noButton(_ sender: UIButton) {
        UIView.animate(withDuration: 1, animations: {
            print(self.chooseView.frame.origin.y)
            self.chooseView.frame.origin.y += self.view.frame.height
            self.buttonView.alpha = 1
            self.returnPic.alpha = 0
        })
    }
    
    //MARK: - Handle the picture
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let pic = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        returnPic.image = pic
        returnPic.alpha = 1
        buttonView.alpha = 0
        chooseView.alpha = 1
        chooseView.frame.origin.y -= self.view.frame.height
        dismiss(animated: true, completion: nil)
    }
        
    //MARK: - Implement dismiss if tap on background
    @objc func backToPrevious() {
        dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! AddViewController
        destinationVC.addPic.image = returnPic.image
    }
}
