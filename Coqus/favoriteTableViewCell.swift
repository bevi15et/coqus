//
//  favoriteTableViewCell.swift
//  Coqus
//
//  Created by Robin Sauma on 2018-11-30.
//  Copyright © 2018 Hassan Al-Baaj. All rights reserved.
//

import UIKit

class favoriteTableViewCell: UITableViewCell {

    @IBOutlet weak var favoriteImageCell: UIImageView!
    @IBOutlet weak var favoriteLabelCell: UILabel!
    
    var id:String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        favoriteImageCell.layer.cornerRadius = favoriteImageCell.frame.height/2
        favoriteImageCell.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
